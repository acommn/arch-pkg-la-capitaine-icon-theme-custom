# Maintainer: Alfonso Commer <acommn@gmail.com>
pkgname=('la-capitaine-light-icon-theme-custom' 'la-capitaine-semi-icon-theme-custom' 'la-capitaine-dark-icon-theme-custom')
pkgbase='la-capitaine-icon-theme-custom'
pkgver=0.6.1
pkgrel=1
epoch=
pkgdesc=
arch=('any')
url="https://github.com/keeferrourke/la-capitaine-icon-theme"
license=('GPL3')
groups=('la-capitaine-icon-theme')
depends=()
makedepends=('gtk-update-icon-cache')
checkdepends=()
optdepends=('gnome-icon-theme' 'breeze-icons' 'elementary-icon-theme' 'deepin-icon-theme')
provides=()
conflicts=('la-capitaine-icon-theme')
replaces=()
backup=()
options=()
install=
changelog=
source=("${pkgbase%-custom}.zip::https://github.com/keeferrourke/${pkgbase%-custom}/archive/v$pkgver.zip")
noextract=('${pkgbase%-custom}.zip')
sha256sums=('20cebcfac364b28d95f66666f1812420b2ccb0f126700fa0e915ddd48daba6f1')

package_la-capitaine-light-icon-theme-custom() {
  pkgdesc="La Capitaine Light is an icon pack designed to integrate with most desktop environments."
  provides=("${pkgname[0]%-custom}")
  cd "$srcdir";
  install --owner=root --group=root --mode=0755 --directory "$pkgdir/usr/share/icons/";
  unzip -q ${pkgbase%-custom}.zip -d "$pkgdir/usr/share/icons/" \
        "${pkgbase%-custom}-$pkgver/actions/*" \
        "${pkgbase%-custom}-$pkgver/animations/*" \
        "${pkgbase%-custom}-$pkgver/apps/*" \
        "${pkgbase%-custom}-$pkgver/devices/*" \
        "${pkgbase%-custom}-$pkgver/emblems/*" \
        "${pkgbase%-custom}-$pkgver/emotes/*" \
        "${pkgbase%-custom}-$pkgver/mimetypes/*" \
        "${pkgbase%-custom}-$pkgver/panel/*" \
        "${pkgbase%-custom}-$pkgver/places/*" \
        "${pkgbase%-custom}-$pkgver/status/*" \
        "${pkgbase%-custom}-$pkgver/configure" \
        "${pkgbase%-custom}-$pkgver/index.theme";
  mv "$pkgdir/usr/share/icons/${pkgbase%-custom}-$pkgver" "$pkgdir/usr/share/icons/${pkgname[0]%-custom}";
  cd "$pkgdir/usr/share/icons/${pkgname[0]%-custom}";
  patch index.theme <<EOF
--- $pkgdir/usr/share/icons/${pkgname[0]%-custom}/index.theme
+++ temp-index.theme
@@ -1,6 +1,6 @@
 [Icon Theme]
-Name=La Capitaine
-Comment=La Capitaine is an icon pack inspired by macOS and Google Material Design - a beautiful hybrid between the two most popular design specifications to date.
+Name=La Capitaine Light
+Comment=La Capitaine Light is an icon pack inspired by macOS and Google Material Design - a beautiful hybrid between the two most popular design specifications to date.
 Inherits=breeze,elementary,gnome,deepin
 Directories=actions/symbolic,actions/22x22,animations/22x22,apps/scalable,apps/symbolic,devices/symbolic,devices/scalable,emblems/scalable,emblems/symbolic,emotes/scalable,mimetypes/scalable,mimetypes/symbolic,places/16x16,places/scalable,places/symbolic,panel/16,panel/24,status/scalable,status/symbolic
EOF
  ./configure;
  gtk-update-icon-cache .;
  rm configure;
}

package_la-capitaine-semi-icon-theme-custom() {
  pkgdesc="La Capitaine Semi is an icon pack designed to integrate with most desktop environments."
  provides=("${pkgname[1]%-custom}")
  cd "$srcdir";
  install --owner=root --group=root --mode=0755 --directory "$pkgdir/usr/share/icons/";
  unzip -q ${pkgbase%-custom}.zip -d "$pkgdir/usr/share/icons/" \
        "${pkgbase%-custom}-$pkgver/actions/*" \
        "${pkgbase%-custom}-$pkgver/animations/*" \
        "${pkgbase%-custom}-$pkgver/apps/*" \
        "${pkgbase%-custom}-$pkgver/devices/*" \
        "${pkgbase%-custom}-$pkgver/emblems/*" \
        "${pkgbase%-custom}-$pkgver/emotes/*" \
        "${pkgbase%-custom}-$pkgver/mimetypes/*" \
        "${pkgbase%-custom}-$pkgver/panel/*" \
        "${pkgbase%-custom}-$pkgver/places/*" \
        "${pkgbase%-custom}-$pkgver/status/*" \
        "${pkgbase%-custom}-$pkgver/configure" \
        "${pkgbase%-custom}-$pkgver/index.theme";
  mv "$pkgdir/usr/share/icons/${pkgbase%-custom}-$pkgver" "$pkgdir/usr/share/icons/${pkgname[1]%-custom}";
  cd "$pkgdir/usr/share/icons/${pkgname[1]%-custom}";
  patch index.theme <<EOF
--- $pkgdir/usr/share/icons/${pkgname[1]%-custom}/index.theme
+++ temp-index.theme
@@ -1,6 +1,6 @@
 [Icon Theme]
-Name=La Capitaine
-Comment=La Capitaine is an icon pack inspired by macOS and Google Material Design - a beautiful hybrid between the two most popular design specifications to date.
+Name=La Capitaine Semi
+Comment=La Capitaine Semi is an icon pack inspired by macOS and Google Material Design - a beautiful hybrid between the two most popular design specifications to date.
 Inherits=breeze,elementary,gnome,deepin
 Directories=actions/symbolic,actions/22x22,animations/22x22,apps/scalable,apps/symbolic,devices/symbolic,devices/scalable,emblems/scalable,emblems/symbolic,emotes/scalable,mimetypes/scalable,mimetypes/symbolic,places/16x16,places/scalable,places/symbolic,panel/16,panel/24,status/scalable,status/symbolic
EOF
  ./configure;
  gtk-update-icon-cache .;
  rm configure;
}

package_la-capitaine-dark-icon-theme-custom() {
  pkgdesc="La Capitaine Dark is an icon pack designed to integrate with most desktop environments."
  provides=("${pkgname[2]%-custom}")
  cd "$srcdir";
  install --owner=root --group=root --mode=0755 --directory "$pkgdir/usr/share/icons/";
  unzip -q ${pkgbase%-custom}.zip -d "$pkgdir/usr/share/icons/" \
        "${pkgbase%-custom}-$pkgver/actions/*" \
        "${pkgbase%-custom}-$pkgver/animations/*" \
        "${pkgbase%-custom}-$pkgver/apps/*" \
        "${pkgbase%-custom}-$pkgver/devices/*" \
        "${pkgbase%-custom}-$pkgver/emblems/*" \
        "${pkgbase%-custom}-$pkgver/emotes/*" \
        "${pkgbase%-custom}-$pkgver/mimetypes/*" \
        "${pkgbase%-custom}-$pkgver/panel/*" \
        "${pkgbase%-custom}-$pkgver/places/*" \
        "${pkgbase%-custom}-$pkgver/status/*" \
        "${pkgbase%-custom}-$pkgver/configure" \
        "${pkgbase%-custom}-$pkgver/index.theme";
  mv "$pkgdir/usr/share/icons/${pkgbase%-custom}-$pkgver" "$pkgdir/usr/share/icons/${pkgname[2]%-custom}";
  cd "$pkgdir/usr/share/icons/${pkgname[2]%-custom}";
  patch index.theme <<EOF
--- $pkgdir/usr/share/icons/${pkgname[2]%-custom}/index.theme
+++ temp-index.theme
@@ -1,6 +1,6 @@
 [Icon Theme]
-Name=La Capitaine
-Comment=La Capitaine is an icon pack inspired by macOS and Google Material Design - a beautiful hybrid between the two most popular design specifications to date.
+Name=La Capitaine Dark
+Comment=La Capitaine Dark is an icon pack inspired by macOS and Google Material Design - a beautiful hybrid between the two most popular design specifications to date.
 Inherits=breeze,elementary,gnome,deepin
 Directories=actions/symbolic,actions/22x22,animations/22x22,apps/scalable,apps/symbolic,devices/symbolic,devices/scalable,emblems/scalable,emblems/symbolic,emotes/scalable,mimetypes/scalable,mimetypes/symbolic,places/16x16,places/scalable,places/symbolic,panel/16,panel/24,status/scalable,status/symbolic
EOF
  ./configure;
  gtk-update-icon-cache .;
  rm configure;
}

# vim:set ts=2 sw=2 et:
